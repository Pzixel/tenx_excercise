# tenx_excercise

Because of using `decimal` type for working with money, this project has **decimal** crate dependency, which requires installed GCC (MinGW added to the PATH in windows case).

Works on stable compiler on Windows, Unix or Mac.

Compiles via `cargo build --release` into `./target/release/tenx_excercise.exe`