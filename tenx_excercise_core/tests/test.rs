extern crate chrono;
extern crate tenx_excercise_core;
#[macro_use]
extern crate decimal;

use chrono::{Datelike, Timelike};
use std::str::FromStr;

use chrono::prelude::*;
use std::borrow::Cow;
use tenx_excercise_core::contract::*;
use tenx_excercise_core::*;

#[test]
fn price_update() {
    let input = "2017-11-01T09:42:23+00:00 KRAKEN BTC USD 1000.0 0.0009";
    let parse_result = ParseResult::from_str(input).unwrap();
    match parse_result {
        ParseResult::PriceUpdate(price_update) => {
            assert_eq!(price_update.timestamp.year(), 2017);
            assert_eq!(price_update.timestamp.month(), 11);
            assert_eq!(price_update.timestamp.day(), 01);
            assert_eq!(price_update.timestamp.hour(), 09);
            assert_eq!(price_update.timestamp.minute(), 42);
            assert_eq!(price_update.timestamp.second(), 23);
            assert_eq!(price_update.exchange, "KRAKEN");
            assert_eq!(price_update.source_currency, "BTC");
            assert_eq!(price_update.destination_currency, "USD");
            assert_eq!(price_update.forward_factor, d128!(1000.0));
            assert_eq!(price_update.backward_factor, d128!(0.0009));
        }
        _ => unreachable!(),
    }
}

#[test]
fn exchange_rate_request() {
    let parse_result = ParseResult::from_str("EXCHANGE_RATE_REQUEST KRAKEN BTC GDAX USD").unwrap();
    match parse_result {
        ParseResult::ExchangeRateRequest(exchange_rate_request) => {
            assert_eq!(exchange_rate_request.source_exchange, "KRAKEN");
            assert_eq!(exchange_rate_request.source_currency, "BTC");
            assert_eq!(exchange_rate_request.destination_exchange, "GDAX");
            assert_eq!(exchange_rate_request.destination_currency, "USD");
        }
        _ => unreachable!(),
    }
}

#[test]
fn path() {
    let price_update = PriceUpdate {
        timestamp: DateTime::<Utc>::from_str("2017-11-01T09:42:23+00:00").unwrap(),
        exchange: "KRAKEN".into(),
        source_currency: "BTC".into(),
        destination_currency: "USD".into(),
        forward_factor: d128!(1000.0),
        backward_factor: d128!(0.0009),
    };
    let price_update2 = PriceUpdate {
        timestamp: DateTime::<Utc>::from_str("2017-11-01T09:43:23+00:00").unwrap(),
        exchange: "GDAX".into(),
        source_currency: "BTC".into(),
        destination_currency: "USD".into(),
        forward_factor: d128!(1001.0),
        backward_factor: d128!(0.0008),
    };
    let exchange_rate_request = ExchangeRateRequest {
        source_exchange: "KRAKEN".into(),
        source_currency: "BTC".into(),
        destination_exchange: "GDAX".into(),
        destination_currency: "USD".into(),
    };

    let mut exchange = CryptoCurrencyExchange::new();
    exchange.update_exchange_rate(&price_update);
    exchange.update_exchange_rate(&price_update2);

    let path = exchange.best_rate_path(&exchange_rate_request).unwrap();
    assert_eq!(
        path.vertices,
        [
            (Cow::Borrowed("KRAKEN"), Cow::Borrowed("BTC")),
            (Cow::Borrowed("GDAX"), Cow::Borrowed("BTC")),
            (Cow::Borrowed("GDAX"), Cow::Borrowed("USD"))
        ]
    );
    assert_eq!(path.rate, d128!(1001.0));
}

#[test]
fn path_format() {
    let exchange_rate_request = ExchangeRateRequest {
        source_exchange: "KRAKEN".into(),
        source_currency: "BTC".into(),
        destination_exchange: "GDAX".into(),
        destination_currency: "USD".into(),
    };

    let path = Path {
        request: &exchange_rate_request,
        vertices: vec![
            (Cow::Borrowed("KRAKEN"), Cow::Borrowed("BTC")),
            (Cow::Borrowed("GDAX"), Cow::Borrowed("BTC")),
            (Cow::Borrowed("GDAX"), Cow::Borrowed("USD")),
        ],
        rate: d128!(1001.0),
    };

    const EXPECTED_RESULT: &'static str = "BEST_RATES_BEGIN KRAKEN BTC GDAX USD 1001.0
KRAKEN BTC
GDAX BTC
GDAX USD
BEST_RATES_END
";

    assert_eq!(format!("{}", path), EXPECTED_RESULT);
}

#[test]
fn test_adjacency_matrix() {
    let mut matrix: AdjacencyMatrix<i32> = AdjacencyMatrix::new(2);
    matrix[[1, 0]] = 10;
    matrix[[0, 1]] = 20;

    assert_eq!(matrix[[1, 0]], 10);
    assert_eq!(matrix[[0, 1]], 20);
}
