extern crate chrono;
#[macro_use]
extern crate decimal;
extern crate itertools;

pub mod contract;

use contract::*;
use decimal::d128;
use itertools::Itertools;
use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::fmt::{Debug, Error, Formatter};
use std::ops::{Index, IndexMut};

pub struct CryptoCurrencyExchange<'a> {
    edges: AdjacencyMatrix<Option<d128>>,
    vertices: HashMap<(Cow<'a, str>, Cow<'a, str>), usize>,
    same_currency_vertices: HashMap<String, Vec<usize>>,
}

impl<'a> CryptoCurrencyExchange<'a> {
    pub fn new() -> Self {
        Self {
            edges: AdjacencyMatrix::default(),
            vertices: HashMap::new(),
            same_currency_vertices: HashMap::new(),
        }
    }

    pub fn update_exchange_rate(&mut self, price_update: &PriceUpdate) {
        let source_exchange = self.get_or_insert_exchange(&price_update.exchange, &price_update.source_currency);
        let destination_exchange =
            self.get_or_insert_exchange(&price_update.exchange, &price_update.destination_currency);
        self.edges[[source_exchange, destination_exchange]] = Some(price_update.forward_factor);
        self.edges[[destination_exchange, source_exchange]] = Some(price_update.backward_factor);
    }

    fn get_or_insert_exchange(&mut self, exchange: &str, currency: &str) -> usize {
        if let Some(position) = self.vertices.get(&(Cow::Borrowed(exchange), Cow::Borrowed(currency))) {
            return *position;
        }

        // Resizing and copying on every new node may lead to performance degradation
        // However, I expect new currencies and exchanges are unlikely to appear frequently
        // It also isn't this bad because it's simply a memcpy instead of copying every thing one by one

        let position = self.vertices.len();
        self.edges.grow(1);
        self.vertices
            .insert((Cow::Owned(exchange.into()), Cow::Owned(currency.into())), position);

        let entry = self.same_currency_vertices.entry(currency.into());

        match entry {
            Entry::Occupied(mut entry) => {
                let entry = entry.get_mut();
                for i in entry.iter() {
                    self.edges[[*i, position]] = Some(d128!(1));
                    self.edges[[position, *i]] = Some(d128!(1));
                }

                entry.push(position);
            }
            Entry::Vacant(entry) => {
                entry.insert(vec![position]);
            }
        }

        position
    }

    pub fn best_rate_path<'b>(&self, request: &'b ExchangeRateRequest) -> Result<Path<'a, 'b>, &'static str> {
        let (next_matrix, rate_matrix) = self.best_rates();

        let mut vertices: Vec<(Cow<'a, str>, Cow<'a, str>)> = Vec::new();

        let u = *self.vertices
            .get(&(
                Cow::Borrowed(&request.source_exchange),
                Cow::Borrowed(&request.source_currency),
            ))
            .ok_or("Don't have vertix for source exchaange and currency")?;
        let v = *self.vertices
            .get(&(
                Cow::Borrowed(&request.destination_exchange),
                Cow::Borrowed(&request.destination_currency),
            ))
            .ok_or("Don't have vertix for destination exchaange and currency")?;

        let mut rate = d128!(1);
        if next_matrix[[u, v]].is_some() {
            let mut u = u;
            loop {
                let value = self.vertices.iter().find(|x| *x.1 == u).unwrap().0;
                vertices.push(value.clone());
                if u == v {
                    break;
                }

                let next_u = next_matrix[[u, v]].unwrap();
                rate *= rate_matrix[[u, next_u]];
                u = next_u;
            }
        }
        Ok(Path {
            request,
            vertices,
            rate,
        })
    }

    fn best_rates(&self) -> (AdjacencyMatrix<Option<usize>>, AdjacencyMatrix<d128>) {
        let n = self.vertices.len();
        let mut rate: AdjacencyMatrix<d128> = AdjacencyMatrix::new(n);
        let mut next: AdjacencyMatrix<Option<usize>> = AdjacencyMatrix::new(n);

        for u in 0..n {
            for v in 0..n {
                if let Some(value) = self.edges[[u, v]] {
                    rate[[u, v]] = value;
                    next[[u, v]] = Some(v);
                }
            }
        }
        for k in 0..n {
            for i in 0..n {
                for j in 0..n {
                    let new_path_value = rate[[i, k]] * rate[[k, j]];
                    if rate[[i, j]] < new_path_value {
                        rate[[i, j]] = new_path_value;
                        next[[i, j]] = next[[i, k]];
                    }
                }
            }
        }

        (next, rate)
    }
}

impl<'a> Debug for CryptoCurrencyExchange<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        writeln!(f, "Adjacency matrix")?;
        for i in 0..self.vertices.len() {
            for j in 0..self.vertices.len() {
                match self.edges[[i, j]] {
                    Some(value) => write!(f, "{:7.7} ", value)?,
                    None => write!(f, "None    ")?,
                }
            }
            writeln!(f)?;
        }
        writeln!(f, "Vertices")?;
        let vec: Vec<_> = self.vertices
            .iter()
            .sorted_by(|a, b| Ord::cmp(&a.1, &b.1))
            .into_iter()
            .map(|x| x.0)
            .collect();
        writeln!(f, "{:?}", vec)?;
        Ok(())
    }
}

pub trait Index2D {
    fn get_indices(&self) -> &[usize; 2];
}

impl Index2D for [usize; 2] {
    fn get_indices(&self) -> &[usize; 2] {
        self
    }
}

pub struct AdjacencyMatrix<T> {
    n: usize,
    value: Vec<T>,
}

impl<T: Default + Clone> AdjacencyMatrix<T> {
    pub fn new(n: usize) -> Self {
        Self {
            n,
            value: vec![T::default(); n * n],
        }
    }

    pub fn grow(&mut self, n: usize) {
        self.n += n;
        let newlen = self.value.len() + 2 * self.n - 1;
        self.value.resize(newlen, T::default());
    }
}

impl<T> Default for AdjacencyMatrix<T> {
    fn default() -> Self {
        Self {
            n: 0,
            value: Vec::new(),
        }
    }
}

impl<T, Idx: Index2D> Index<Idx> for AdjacencyMatrix<T> {
    type Output = T;

    fn index(&self, index: Idx) -> &<Self as Index<Idx>>::Output {
        let indices = index.get_indices();
        let linear_index = index_linear(indices[0], indices[1]);
        self.value.index(linear_index)
    }
}

impl<T, Idx: Index2D> IndexMut<Idx> for AdjacencyMatrix<T> {
    fn index_mut(&mut self, index: Idx) -> &mut <Self as Index<Idx>>::Output {
        let indices = index.get_indices();
        let linear_index = index_linear(indices[0], indices[1]);
        self.value.index_mut(linear_index)
    }
}

fn index_linear(i: usize, j: usize) -> usize {
    let s = i.max(j);
    if s % 2 == 0 {
        (s * s + s) + i - j
    } else {
        (s * s + s) + j - i
    }
}

impl<T: Debug> Debug for AdjacencyMatrix<T> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        writeln!(f, "Adjacency matrix")?;
        for i in 0..self.n {
            for j in 0..self.n {
                write!(f, "{:?} ", self[[i, j]])?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
