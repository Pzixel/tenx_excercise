use chrono::prelude::*;
use decimal::*;
use std::borrow::Cow;
use std::error::Error;
use std::fmt::Display;
use std::fmt::Error as FmtError;
use std::fmt::Formatter;
use std::str::FromStr;

#[derive(Debug)]
pub struct PriceUpdate {
    pub timestamp: DateTime<Utc>,
    pub exchange: String,
    pub source_currency: String,
    pub destination_currency: String,
    pub forward_factor: d128,
    pub backward_factor: d128,
}

#[derive(Debug)]
pub struct ExchangeRateRequest {
    pub source_exchange: String,
    pub source_currency: String,
    pub destination_exchange: String,
    pub destination_currency: String,
}

pub enum ParseResult {
    PriceUpdate(PriceUpdate),
    ExchangeRateRequest(ExchangeRateRequest),
}

impl FromStr for ParseResult {
    type Err = Cow<'static, str>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split_whitespace().collect();

        if split.len() == 5 && split[0] == "EXCHANGE_RATE_REQUEST" {
            let result = ExchangeRateRequest {
                source_exchange: split[1].into(),
                source_currency: split[2].into(),
                destination_exchange: split[3].into(),
                destination_currency: split[4].into(),
            };

            return Ok(ParseResult::ExchangeRateRequest(result));
        }

        if split.len() == 6 {
            let timestamp = DateTime::<Utc>::from_str(split[0]).map_err(|e| Cow::from(e.description().to_string()))?;
            let forward_factor = d128::from_str(split[4]).map_err(|_| Cow::from("Invalid float format"))?;
            let backward_factor = d128::from_str(split[5]).map_err(|_| Cow::from("Invalid float format"))?;
            let result = PriceUpdate {
                timestamp,
                exchange: split[1].into(),
                source_currency: split[2].into(),
                destination_currency: split[3].into(),
                forward_factor,
                backward_factor,
            };

            return Ok(ParseResult::PriceUpdate(result));
        }

        Err("Unknown format".into())
    }
}

#[derive(Debug)]
pub struct Path<'a, 'b> {
    pub request: &'b ExchangeRateRequest,
    pub vertices: Vec<(Cow<'a, str>, Cow<'a, str>)>,
    pub rate: d128,
}

impl<'a, 'b> Display for Path<'a, 'b> {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        writeln!(
            f,
            "BEST_RATES_BEGIN {} {} {} {} {}",
            self.request.source_exchange,
            self.request.source_currency,
            self.request.destination_exchange,
            self.request.destination_currency,
            self.rate
        )?;
        for vertex in self.vertices.iter() {
            writeln!(f, "{} {}", vertex.0, vertex.1)?
        }
        writeln!(f, "BEST_RATES_END")?;
        Ok(())
    }
}
