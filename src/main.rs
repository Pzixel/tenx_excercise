extern crate tenx_excercise_core;

use std::error::Error;
use std::io;
use std::str::FromStr;
use tenx_excercise_core::contract::*;
use tenx_excercise_core::*;

fn main() {
    let mut exchange = CryptoCurrencyExchange::new();

    loop {
        if let Err(e) = process_line(&mut exchange) {
            eprintln!("{}", e);
        };
    }
}

fn process_line(exchange: &mut CryptoCurrencyExchange) -> Result<(), String> {
    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .map_err(|e| format!("Error occured while reading from stdin {}", e.description()))?;

    let parse_result = ParseResult::from_str(&line).map_err(|e| format!("Error occured while parsing request {}", e))?;

    match parse_result {
        ParseResult::PriceUpdate(price_update) => exchange.update_exchange_rate(&price_update),
        ParseResult::ExchangeRateRequest(exchange_rate_request) => {
            let path = exchange
                .best_rate_path(&exchange_rate_request)
                .map_err(|e| e.to_string())?;
            println!("{}", path);
        }
    }
    Ok(())
}
